# Sciami Molino `puredata` patch

These are instructions concerning the use of this `puredata` patch. Feel free
to add to it if you are so inclined.

## Usage

### Start

The best way to run this patch is to run `puredata` from a terminal emulator
(command line). The line is:

```sh
$ pd main.pd &
```

### Jack configuration

As configured, this patch outputs sound over 13 channels (1-8 octophonic, 9-12
4 mono dumps, 13 mono reverb send). If you want to hear these channels through,
say, your stereo headphones you have to connect the outlets to your left-right
system sinks.
