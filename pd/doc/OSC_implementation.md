# Implementazione dei messaggi OSC

| descrizione | send | rcv | valori ammissibili | annotazioni |
|:----:|:----:|:----:|:----:|:----:|:----:|
| Master 8 canali | /sciami/master_8ch_rcv | | 0-127 |
| Master canale mono 1 | /sciami/master_1_rcv | | 0-127 |
| Master canale mono 2 | /sciami/master_2_rcv | | 0-127 |
| Master canale mono 3 | /sciami/master_3_rcv | | 0-127 |
| Master canale mono 4 | /sciami/master_4_rcv | | 0-127 |
| Windowing type | /sciami/wtype | | 0-1 | (0: rettangolare, 1: hanning) |
| Sciame         | /sciami/tipo_sciame | | 0-n |dove n è il numero totale di sciami (0 = sciami OFF)|
| Sciame on/off  | /sciami/sciame\_onoff | | 0-1 | accensione/spegnimento dei generatori |
| Rand min       | /sciami/rand_min    | | | offset del random di distanza tra grani |
| Rand max       | /sciami/rand_max    | | | massimo del random di distanza tra grani |
